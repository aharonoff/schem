var tutorialTiles = [
  [0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [1, 1, 1, 1, 1],
  [0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0]
]

var shrinkTiles = [
  [0, 0, 0, 0, 0],
  [0, 1, 1, 1, 0],
  [0, 1, 1, 1, 0],
  [0, 1, 1, 1, 0],
  [0, 0, 0, 0, 0]
]

var disappearTiles = [
  [0, 0, 1],
  [1, 0, 1],
  [2, 0, 1],
  [3, 0, 1],
  [4, 0, 1],
  [4, 1, 1],
  [4, 2, 1],
  [4, 3, 1],
  [4, 4, 1],
  [3, 4, 1],
  [2, 4, 1],
  [1, 4, 1],
  [0, 4, 1],
  [0, 3, 1],
  [0, 2, 1],
  [0, 1, 1]
]

var animationState = {
  title: false,
  intro: false,
  move: false,
  grab: false,
  drop: false,
  center: false,
  shrink: false,
  disappear: false,
  guidelines: false,
  togo: false,
  combo: false,
  collision: false,
  over: false,
  endscreen: false
}

var animation = {
  title: 0,
  enter: 0,
  intro: 0,
  move: 0,
  grab: 0,
  drop: 0,
  center: 0,
  shrink: 0,
  disappear: 0,
  guidelines: 0,
  restart: 0,
  togo: 0,
  combo: 0,
  collision: 0,
  resume: 0,
  over: 0,
  endscreen: 0
}

var speed = {
  title: 0.00025,
  enter: 0.0025,
  intro: 0.0005,
  move: 0.000375,
  grab: 0.00025,
  drop: 0.00025,
  center: 0.00025,
  shrink: 0.00025,
  disappear: 0.0025,
  guidelines: 0.00025,
  scaling: 0.0005,
  restart: 0.0025,
  togo: 0.05,
  combo: 0.05,
  collision: 0.05,
  resume: 0.0025,
  over: 0.001,
  endscreen: 0.001
}

var used = {
  intro: false,
  move: false,
  grab: false,
  reset: false
}

function drawTile(size) {
  beginShape()
  vertex(-0.5 * size, -0.5 * size)
  vertex(0.5 * size, -0.5 * size)
  vertex(0.5 * size, 0.5 * size)
  vertex(-0.5 * size, 0.5 * size)
  endShape(CLOSE)
}

function drawLetters(letter, size, state) {
  if (letter.toLowerCase() === 'a') {
    beginShape()
    vertex((-0.5 - state * 2) * size, -2.5 * size)
    vertex((0.5 + state * 2) * size, -2.5 * size)
    vertex((0.5 + state * 2) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, -0.5 * size)
    vertex((-1.5 - state * 1) * size, -0.5 * size)
    vertex((-1.5 - state * 1) * size, -1.5 * size)
    vertex((-0.5 - state * 2) * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'b') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'c') {
    beginShape()
    vertex(size * -2.5, size * -2.5)
    vertex(size * 2.5, size * -2.5)
    vertex(size * 2.5, size * -0.5)
    vertex(size * (1.5 + state * 1), size * -0.5)
    vertex(size * (1.5 + state * 1), size * (-1.5 + state * 1))
    vertex(size * (-1.5 + state * 4), size * (-1.5 + state * 1))
    vertex(size * (-1.5 + state * 4), size * (1.5 - state * 2))
    vertex(size * (1.5 + state * 1), size * (1.5 - state * 2))
    vertex(size * (1.5 + state * 1), size * (0.5 - state * 1))
    vertex(size * 2.5, size * (0.5 - state * 1))
    vertex(size * 2.5, size * 2.5)
    vertex(size * -2.5, size * 2.5)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'd') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (1.5 +  state * 1) * size)
    vertex((1.5 + state * 1) * size, (1.5 + state * 1) * size)
    vertex((1.5 + state * 1) * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'e') {
    beginShape()
    vertex(size * -2.5, size * -2.5)
    vertex(size * 2.5, size * -2.5)
    vertex(size * 2.5, size * -1.5)
    vertex(size * (-1.5 + state * 4), size * -1.5)
    vertex(size * (-1.5 + state * 4), size * -0.5)
    vertex(size * (0.5 + state * 2), size * -0.5)
    vertex(size * (0.5 + state * 2), size * 0.5)
    vertex(size * (-1.5 + state * 4), size * 0.5)
    vertex(size * (-1.5 + state * 4), size * 1.5)
    vertex(size * 2.5, size * 1.5)
    vertex(size * 2.5, size * 2.5)
    vertex(size * -2.5, size * 2.5)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'f') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((-1.5 + state * 4) * size, -1.5 * size)
    vertex((-1.5 + state * 4) * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((-1.5 +  state * 4) * size, 0.5 * size)
    vertex((-1.5 +  state * 4) * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'g') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((-1.5 +  state * 4) * size, -1.5 * size)
    vertex((-1.5 + state * 4) * size, (1.5 - state * 2) * size)
    vertex((1.5 + state * 1) * size, (1.5 - state * 2) * size)
    vertex((1.5 + state * 1) * size, (0.5 - state * 1) * size)
    vertex((0.5 +  state * 2) * size, (0.5 - state * 1) * size)
    vertex((0.5 + state * 2) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'h') {
    beginShape()
    vertex(size * -2.5, size * -2.5)
    vertex(size * -1.5, size * -2.5)
    vertex(size * -1.5, size * (-0.5 - state * 2))
    vertex(size * 1.5, size * (-0.5 - state * 2))
    vertex(size * 1.5, size * -2.5)
    vertex(size * 2.5, size * -2.5)
    vertex(size * 2.5, size * 2.5)
    vertex(size * 1.5, size * 2.5)
    vertex(size * 1.5, size * (0.5 + state * 2))
    vertex(size * -1.5, size * (0.5 + state * 2))
    vertex(size * -1.5, size * 2.5)
    vertex(size * -2.5, size * 2.5)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'i') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((0.5 + state * 2) * size, -1.5 * size)
    vertex((0.5 + state * 2) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((-0.5 - state * 2) * size, 1.5 * size)
    vertex((-0.5 - state * 2) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'j') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, (0.5 - state * 0.5) * size)
    vertex((-1.5 - state * 1) * size, (0.5 - state * 0.5) * size)
    vertex((-1.5 - state * 1) * size, (1.5 - state * 1.5) * size)
    vertex((1.5 - state * 4) * size, (1.5 - state * 1.5) * size)
    vertex((1.5 - state * 4) * size, -1.5 * size)
    vertex((-1.5 - state * 1) * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'k') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'l') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex((-1.5 + state * 4) * size, -2.5 * size)
    vertex((-1.5 + state * 4) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'm') {
    beginShape()
    vertex(size * -2.5, size * -2.5)
    vertex(size * -1.5, size * -2.5)
    vertex(size * -1.5, size * (-1.5 - state * 1))
    vertex(size * -0.5, size * (-1.5 - state * 1))
    vertex(size * -0.5, size * (-0.5 - state * 2))
    vertex(size * 0.5, size * (-0.5 - state * 2))
    vertex(size * 0.5, size * (-1.5 - state * 1))
    vertex(size * 1.5, size * (-1.5 - state * 1))
    vertex(size * 1.5, size * -2.5)
    vertex(size * 2.5, size * -2.5)
    vertex(size * 2.5, size * 2.5)
    vertex(size * 1.5, size * 2.5)
    vertex(size * 1.5, size * (-0.5 + state * 3))
    vertex(size * 0.5, size * (-0.5 + state * 3))
    vertex(size * 0.5, size * (0.5 + state * 2))
    vertex(size * -0.5, size * (0.5 + state * 2))
    vertex(size * -0.5, size * (-0.5 + state * 3))
    vertex(size * -1.5, size * (-0.5 + state * 3))
    vertex(size * -1.5, size * 2.5)
    vertex(size * -2.5, size * 2.5)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'n') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (-1.5 - state * 1) * size)
    vertex(-0.5 * size, (-1.5 - state * 1) * size)
    vertex(-0.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (0.5 - state * 3) * size)
    vertex(1.5 * size, (0.5 - state * 3) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (0.5 + state * 2) * size)
    vertex(-0.5 * size, (0.5 + state * 2) * size)
    vertex(-0.5 * size, (-0.5 + state * 3) * size)
    vertex(-1.5 * size, (-0.5 + state * 3) * size)
    vertex(-1.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'o') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (1.5 + state * 1) * size)
    vertex((1.5 + state * 1) * size, (1.5 + state * 1) * size)
    vertex((1.5 + state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (-1.5 - state * 1) * size)
    vertex((-1.5 - state * 1) * size, (-1.5 - state * 1) * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'p') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, (0.5 + state * 2) * size)
    vertex((-1.5 + state * 4) * size, (0.5 + state * 2) * size)
    vertex((-1.5 + state * 4) * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'q') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (-1.5 - state * 1) * size)
    vertex((-1.5 - state * 1) * size, (-1.5 - state * 1) * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'r') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 's') {
    beginShape()
    vertex(size * -2.5, size * -2.5)
    vertex(size * 2.5, size * -2.5)
    vertex(size * 2.5, size * -1.5)
    vertex(size * (-1.5 + state * 4), size * -1.5)
    vertex(size * (-1.5 + state * 4), size * -0.5)
    vertex(size * 2.5, size * -0.5)
    vertex(size * 2.5, size * 2.5)
    vertex(size * -2.5, size * 2.5)
    vertex(size * -2.5, size * 1.5)
    vertex(size * (1.5 - state * 4), size * 1.5)
    vertex(size * (1.5 - state * 4), size * 0.5)
    vertex(size * -2.5, size * 0.5)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 't') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((0.5 + state * 2) * size, -1.5 * size)
    vertex((0.5 + state * 2) * size, 2.5 * size)
    vertex((-0.5 - state * 2) * size, 2.5 * size)
    vertex((-0.5 - state * 2) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'u') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (1.5 - state * 4) * size)
    vertex(1.5 * size, (1.5 - state * 4) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'v') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (-0.5 - state * 2) * size)
    vertex(-0.5 * size, (-0.5 - state * 2) * size)
    vertex(-0.5 * size, (0.5 - state * 3) * size)
    vertex(0.5 * size, (0.5 - state * 3) * size)
    vertex(0.5 * size, (-0.5 - state * 2) * size)
    vertex(1.5 * size, (-0.5 - state * 2) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, (0.5 + state * 2) * size)
    vertex(1.5 * size, (0.5 + state * 2) * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, 2.5 * size)
    vertex(-0.5 * size, 2.5 * size)
    vertex(-0.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, (0.5 + state * 2) * size)
    vertex(-2.5 * size, (0.5 + state * 2) * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'w') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (1.5 - state * 4) * size)
    vertex(-0.5 * size, (1.5 - state * 4) * size)
    vertex(-0.5 * size, (-1.5 - state * 1) * size)
    vertex(0.5 * size, (-1.5 - state * 1) * size)
    vertex(0.5 * size, (1.5 - state * 4) * size)
    vertex(1.5 * size, (1.5 - state * 4) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'x') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (-1.5 - state * 1) * size)
    vertex(-0.5 * size, (-1.5 - state * 1) * size)
    vertex(-0.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (-0.5 - state * 2) * size)
    vertex(0.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((1.5 + state * 1) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(0.5 * size, (0.5 + state * 2) * size)
    vertex(-0.5 * size, (0.5 + state * 2) * size)
    vertex(-0.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, (1.5 + state * 1) * size)
    vertex(-1.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((-1.5 - state * 1) * size, 1.5 * size)
    vertex((-1.5 - state * 1) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, -0.5 * size)
    vertex((-1.5 - state * 1) * size, -0.5 * size)
    vertex((-1.5 - state * 1) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'y') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (-0.5 - state * 2) * size)
    vertex(1.5 * size, (-0.5 - state * 2) * size)
    vertex(1.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, (-0.5 + state * 3) * size)
    vertex(1.5 * size, (-0.5 + state * 3) * size)
    vertex(1.5 * size, (0.5 + state * 2) * size)
    vertex(0.5 * size, (0.5 + state * 2) * size)
    vertex(0.5 * size, 2.5 * size)
    vertex(-0.5 * size, 2.5 * size)
    vertex(-0.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, (0.5 + state * 2) * size)
    vertex(-1.5 * size, (-0.5 + state * 3) * size)
    vertex(-2.5 * size, (-0.5 + state * 3) * size)
    endShape(CLOSE)
  }
  if (letter.toLowerCase() === 'z') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, 0.5 * size)
    vertex((-0.5 + state * 3) * size, 0.5 * size)
    vertex((-0.5 + state * 3) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((-1.5 - state * 1) * size, 1.5 * size)
    vertex((-1.5 - state * 1) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, -0.5 * size)
    vertex((0.5 - state * 3) * size, -0.5 * size)
    vertex((0.5 - state * 3) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter === '0') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (-1.5 - state * 1) * size)
    vertex(2.5 * size, (1.5 + state * 1) * size)
    vertex((1.5 + state * 1) * size, (1.5 + state * 1) * size)
    vertex((1.5 + state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (-1.5 - state * 1) * size)
    vertex((-1.5 - state * 1) * size, (-1.5 - state * 1) * size)
    endShape(CLOSE)
  }
  if (letter === '1') {
    beginShape()
    vertex((-0.5 - state * 2) * size, -2.5 * size)
    vertex((0.5 + state * 2) * size, -2.5 * size)
    vertex((0.5 + state * 2) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((-0.5 - state * 2) * size, 1.5 * size)
    vertex((-0.5 - state * 2) * size, -0.5 * size)
    vertex(-2.5 * size, -0.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    vertex((-0.5 - state * 2) * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter === '2') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex((-1.5 + state * 4) * size, 0.5 * size)
    vertex((-1.5 + state * 4) * size, 1.5 * size)
    vertex(2.5 * size, 1.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 0.5 * size)
    vertex((-1.5 - state * 1) * size, 0.5 * size)
    vertex((-1.5 - state * 1) * size, -0.5 * size)
    vertex((1.5 - state * 4) * size, -0.5 * size)
    vertex((1.5 - state * 4) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter === '3') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((1.5 + state * 1) * size, 0.5 * size)
    vertex(2.5 * size, 0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, -0.5 * size)
    vertex((1.5 - state * 4) * size, -0.5 * size)
    vertex((1.5 - state * 4) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter === '4') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(-1.5 * size, -2.5 * size)
    vertex(-1.5 * size, (0.5 - state * 3) * size)
    vertex(0.5 * size, (0.5 - state * 3) * size)
    vertex(0.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, (-1.5 - state * 1) * size)
    vertex(1.5 * size, (0.5 - state * 3) * size)
    vertex(2.5 * size, (0.5 - state * 3) * size)
    vertex(2.5 * size, (1.5 + state * 1) * size)
    vertex(1.5 * size, (1.5 + state * 1) * size)
    vertex(1.5 * size, 2.5 * size)
    vertex(0.5 * size, 2.5 * size)
    vertex(0.5 * size, (1.5 + state * 1) * size)
    vertex(-2.5 * size, (1.5 + state * 1) * size)
    endShape(CLOSE)
  }
  if (letter === '5') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((-0.5 + state * 3) * size, -1.5 * size)
    vertex((-0.5 + state * 3) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 0.5 * size)
    vertex((-1.5 - state * 1) * size, 0.5 * size)
    endShape(CLOSE)
  }
  if (letter === '6') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((-1.5 + state * 4) * size, -1.5 * size)
    vertex((-1.5 + state * 4) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    endShape(CLOSE)
  }
  if (letter === '7') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -1.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, -0.5 * size)
    vertex((0.5 + state * 2) * size, 0.5 * size)
    vertex((-0.5 + state * 3) * size, 0.5 * size)
    vertex((-0.5 + state * 3) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, 0.5 * size)
    vertex((-0.5 - state * 2) * size, -0.5 * size)
    vertex((0.5 - state * 3) * size, -0.5 * size)
    vertex((0.5 - state * 3) * size, -1.5 * size)
    vertex(-2.5 * size, -1.5 * size)
    endShape(CLOSE)
  }
  if (letter === '8') {
    beginShape()
    vertex((-1.5 - state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -2.5 * size)
    vertex((1.5 + state * 1) * size, -0.5 * size)
    vertex(2.5 * size, -0.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex(-2.5 * size, 2.5 * size)
    vertex(-2.5 * size, -0.5 * size)
    vertex((-1.5 - state * 1) * size, -0.5 * size)
    endShape(CLOSE)
  }
  if (letter === '9') {
    beginShape()
    vertex(-2.5 * size, -2.5 * size)
    vertex(2.5 * size, -2.5 * size)
    vertex(2.5 * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 2.5 * size)
    vertex((-1.5 - state * 1) * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 1.5 * size)
    vertex((1.5 - state * 4) * size, 0.5 * size)
    vertex(-2.5 * size, 0.5 * size)
    endShape(CLOSE)
  }
  if (letter === '+') {
    beginShape()
    vertex((-0.5 - 2 * state) * size, -2.5 * size)
    vertex((0.5 + 2 * state) * size, -2.5 * size)
    vertex((0.5 + 2 * state) * size, (-0.5 - 2 * state) * size)
    vertex(2.5 * size, (-0.5 - 2 * state) * size)
    vertex(2.5 * size, (0.5 + 2 * state) * size)
    vertex((0.5 + 2 * state) * size, (0.5 + 2 * state) * size)
    vertex((0.5 + 2 * state) * size, 2.5 * size)
    vertex((-0.5 - 2 * state) * size, 2.5 * size)
    vertex((-0.5 - 2 * state) * size, (0.5 + 2 * state) * size)
    vertex(-2.5 * size, (0.5 + 2 * state) * size)
    vertex(-2.5 * size, (-0.5 - 2 * state) * size)
    vertex((-0.5 - 2 * state) * size, (-0.5 - 2 * state) * size)
    endShape(CLOSE)
  }
}

function drawArrow(size, orientation) {
  push()

  rotate(orientation * Math.PI * 0.5)
  rect(0, 0, size * 0.25, size)

  push()
  translate(-sqrt(2) * size * (0.25 - 0.0625), -size * 0.4)
  rotate(Math.PI * 0.25)
  rect(0, 0, size * 0.25, size)
  pop()

  push()
  translate(sqrt(2) * size * (0.25 - 0.0625), -size * 0.4)
  rotate(-Math.PI * 0.25)
  rect(0, 0, size * 0.25, size)
  pop()

  pop()
}

function makeStar() {
  for (var i = 0; i < factor.numOfStars; i++) {
    stars.push(new Star(factor.randS))
  }
}

class Star {
  constructor(rand) {
    this.x = 0
    this.y = 0
    this.size = 0.0001 + Math.random() * 0.001
    this.growth = 0.00001
    this.dir = Math.random() * 2
    this.randS = Math.random() * rand
    this.speed = 0.005 + this.randS
    this.acc = 1.00 + this.randS * 128
    this.len = 0
    this.lenGrowth = this.randS
  }

  display() {
    noFill()
    stroke(255, 1 - animation.endscreen - animation.over)
    strokeWeight(boardSize * this.size)
    strokeCap(ROUND)
    line(this.x, this.y, this.x - this.len * sin(this.dir * Math.PI) * boardSize, this.y - this.len * cos(this.dir * Math.PI) * boardSize)
  }

  move() {
    this.speed *= this.acc
    this.x += this.speed * sin(this.dir * Math.PI) * boardSize
    this.y += this.speed * cos(this.dir * Math.PI) * boardSize
    this.size += this.growth
    this.lenGrowth *= this.acc
    this.len += this.lenGrowth
  }

  isOffScreen() {
    if (dist(this.x, this.y, 0, 0) >= boardSize) {
      return true
    }
  }
}
