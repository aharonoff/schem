var ARRAY_PROPS = {
  length: 'number',
  sort: 'function',
  slice: 'function',
  splice: 'function'
}
var boardSize
var maxSize = 0.02
var initSize = 0.01
var maxDimension = 9
var initDimension = 3
var player = {
  x: Math.floor(tutorialTiles.length * 0.5),
  y: Math.floor(tutorialTiles.length * 0.5)
}
// var player = {
//   x: Math.floor(maxDimension * 0.5),
//   y: Math.floor(maxDimension * 0.5)
// }
var neighbours = []
var tileArray = create2DArray(maxDimension, maxDimension, 0, true)
var wallArray
var wallEdges
var texts = {
  title: [
    ['e', 'm', 's', 'c', 'h'],
    ['m', 's', 'c', 'h', 'e'],
    ['s', 'c', 'h', 'e', 'm'],
    ['c', 'h', 'e', 'm', 's'],
    ['h', 'e', 'm', 's', 'c']
  ],
  enter: 'press space',
  intro: 'you are a black square',
  move: 'move around with arrows',
  grab: 'now use space + arrows',
  drop: ['center the white tiles', 'into a 3 by 3 arrangement'],
  center: ['move to the center', 'to start the actual game'],
  paused: 'paused',
  resume: 'press p to resume',
  over: ['game', 'over'],
  restart: 'press space to restart',
  combo: 'combo',
  endscreen: ['you', 'won']
}
var states = {
  title: true,
  tutorial: false,
  grabNeighbour: false,
  tileGrabbed: false,
  gameplay: false,
  combo: false,
  over: false,
  focused: false,
  paused: true,
  endscreen: false
}
var scaling
var maxScale

var matches = 0
var combo = false
var matchesForCombo = 4
var tileCount = initDimension * initDimension
var endNumOfTiles = 25
var collided
var additionalTiles
var tileToAdd
var randX = 0
var randY = 0

var factor = {
  frame: 0,
  movement: 0,
  swing: 0,
  shake: 0,
  randS: 0.00025,
  numOfStars: 4
}
var initValue = {
  movement: 0,
  swing: 0,
  shake: 0,
  numOfStars: 4,
  randS: 0.00025,
  scaling: 0.0005
}
var endValue = {
  movement: 13,
  swing: Math.PI * 0.25,
  shake: 0.015,
  numOfStars: 32,
  randS: 0.0025,
  scaling: 0.005
}
var stars = []
var newStar = 0
var numOfLevels = endNumOfTiles - initDimension * initDimension
var initNumOfTiles = initDimension * initDimension

var autoWah

// TODO: add sound effects on actions + generative music (based on tile arrangement?, connected to speed, etc. factors?)
// https://medium.com/dev-red/tutorial-lets-make-music-with-javascript-and-tone-js-f6ac39d95b8c
// https://tonejs.github.io/docs/14.7.77/index.html
// https://tonejs.github.io/examples/
// https://www.adajcc.com/blog/2018/10/29/midterm-live-audio-visual-techno-generator

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    maxScale = windowHeight / (boardSize * initSize) / 6.5
  } else {
    boardSize = windowWidth - 80
    maxScale = windowWidth / (boardSize * initSize) / 6.5
  }
  scaling = maxScale
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < initDimension; i++) {
    for (var j = 0; j < initDimension; j++) {
      tileArray[Math.floor(maxDimension * 0.5 - 1) + i][Math.floor(maxDimension * 0.5 - 1) + j] = 1
    }
  }
  wallArray = deepCopy(tileArray)
  wallEdges = getVertices(wallArray)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(0)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  // title screen
  if (states.title === true) {
    // title
    for (var i = 0; i < texts.title.length; i++) {
      for (var j = 0; j < texts.title[i].length; j++) {
        if (i === Math.floor(texts.title.length * 0.5) || j === Math.floor(texts.title.length * 0.5)) {
          fill(255)
        } else {
          noFill()
        }
        stroke(255)
        strokeWeight(1)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.title.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (j - Math.floor(texts.title[i].length * 0.5)) * boardSize * maxSize * 6.5)
        drawLetters(texts.title[i][j], boardSize * maxSize, animation.title)
        pop()
      }
    }
    // 'press space' text
    if (animationState.title === false) {
      for (var i = 0; i < texts.enter.length; i++) {
        fill(255, 0.5 + 0.5 * sin(animation.enter))
        noStroke()
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.enter.length * 0.5) + ((texts.enter.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 3 * boardSize * maxSize * 6.5)
        drawLetters(texts.enter[i], boardSize * maxSize * 0.25, 0)
        pop()
      }
    }
    animation.enter += deltaTime * speed.enter
    // transition
    if (animationState.title === true) {
      animation.title += deltaTime * speed.title
      if (animation.title >= 0.999) {
        animation.title = 1
        setTimeout(function() {
          animationState.title = false
          states.title = false
          states.tutorial = true
          animationState.intro = true
        }, 500)
      }
    }
  }

  // tutorial
  if (states.tutorial === true) {
    // place indicators
    if (animationState.intro === true) {
      for (var i = 0; i < initDimension; i++) {
        for (var j = 0; j < initDimension; j++) {
          noFill()
          stroke(255 * animation.intro)
          strokeWeight(2)
          push()
          translate(windowWidth * 0.5 + (i - Math.floor(initDimension * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (j - Math.floor(initDimension * 0.5)) * boardSize * maxSize * 6.5)
          drawTile(boardSize * maxSize * 6.5 * 0.95)
          pop()
        }
      }
    } else {
      // place indicators
      for (var i = 0; i < initDimension; i++) {
        for (var j = 0; j < initDimension; j++) {
          noFill()
          stroke(255)
          strokeWeight(2)
          push()
          translate(windowWidth * 0.5 + (i - Math.floor(initDimension * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (j - Math.floor(initDimension * 0.5)) * boardSize * maxSize * 6.5)
          drawTile(boardSize * maxSize * 6.5 * 0.95)
          pop()
        }
      }
    }
    // tiles
    for (var i = 0; i < tutorialTiles.length; i++) {
      for (var j = 0; j < tutorialTiles[i].length; j++) {
        if (tutorialTiles[i][j] === 1) {
          fill(255)
        } else {
          noFill()
        }
        stroke(255)
        strokeWeight(1)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (j - Math.floor(tutorialTiles[i].length * 0.5)) * boardSize * maxSize * 6.5)
        drawTile(boardSize * maxSize * 5)
        pop()
      }
    }

    // intro
    if (animationState.intro === true) {
      for (var i = 0; i < texts.intro.length; i++) {
        noFill()
        stroke(128 + 128 * sin(-Math.PI * 0.5 + Math.PI * animation.intro))
        strokeWeight(1)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.intro.length * 0.5) + ((texts.intro.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 3 * boardSize * maxSize * 6.5)
        drawLetters(texts.intro[i], boardSize * maxSize * 0.25, 0)
        pop()
      }

      // player
      if (animation.intro < 1) {
        push()
        translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
        fill(0)
        stroke(0)
        strokeWeight(1)
        drawTile(boardSize * maxSize * 3 * animation.intro)
        pop()
      } else {
        push()
        translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
        fill(0)
        stroke(0)
        strokeWeight(1)
        drawTile(boardSize * maxSize * 3)
        pop()
      }

      animation.intro += deltaTime * speed.intro
      if (animation.intro >= 1.99) {
        animation.intro = 0
        animationState.intro = false
        animationState.move = true
        used.intro = true
      }
    } else {
      // player
      push()
      translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
      fill(0)
      stroke(0)
      strokeWeight(1)
      drawTile(boardSize * maxSize * 3)
      pop()

      // grabbed tile
      if (states.tileGrabbed === true) {
        push()
        translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
        noStroke()
        fill(255)
        drawTile(boardSize * maxSize * 1)
        pop()
      }

      // neighbour grab arrows
      if (states.grabNeighbour === true) {
        if (neighbours[0] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 + boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 1)
          pop()
        }
        if (neighbours[1] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 - boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 3)
          pop()
        }
        if (neighbours[2] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 + boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 2)
          pop()
        }
        if (neighbours[3] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 - boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 0)
          pop()
        }
      }

      // tile placement arrows
      if (states.tileGrabbed === true) {
        neighbours = getNeighbours(player.x, player.y, tutorialTiles, 0, 0, 1)
        if (neighbours[0] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 + boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 1)
          pop()
        }
        if (neighbours[1] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 - boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 3)
          pop()
        }
        if (neighbours[2] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 + boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 2)
          pop()
        }
        if (neighbours[3] === true) {
          push()
          translate(windowWidth * 0.5 + (Math.floor(player.x) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5, windowHeight * 0.5 + (Math.floor(player.y) - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 - boardSize * maxSize * 6.5)
          fill(128)
          noStroke()
          drawArrow(boardSize * maxSize * 1.25, 0)
          pop()
        }
      }

      // movement
      if (animationState.move === true && used.move === false) {
        for (var i = 0; i < texts.move.length; i++) {
          noFill()
          stroke(128 + 128 * sin(-Math.PI * 0.5 + Math.PI * animation.move))
          strokeWeight(1)
          push()
          translate(windowWidth * 0.5 + (i - Math.floor(texts.move.length * 0.5) + ((texts.move.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 3 * boardSize * maxSize * 6.5)
          drawLetters(texts.move[i], boardSize * maxSize * 0.25, 0)
          pop()
        }

        animation.move += deltaTime * speed.move
        if (animation.move >= 1.99) {
          animation.move = 0
          animationState.move = false
        }
      }

      // tile grabbing
      if (animationState.grab === true && used.grab === false) {
        for (var i = 0; i < texts.grab.length; i++) {
          noFill()
          stroke(128 + 128 * sin(-Math.PI * 0.5 + Math.PI * animation.grab))
          strokeWeight(1)
          push()
          translate(windowWidth * 0.5 + (i - Math.floor(texts.grab.length * 0.5) + ((texts.grab.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 3 * boardSize * maxSize * 6.5)
          drawLetters(texts.grab[i], boardSize * maxSize * 0.25, 0)
          pop()
        }

        animation.grab += deltaTime * speed.grab
        if (animation.grab >= 1.99) {
          animation.grab = 0
          animationState.grab = false
          used.grab = true
        }
      }

      // tile rearrangement
      if (animationState.drop === true && used.reset === false) {
        for (var i = 0; i < texts.drop.length; i++) {
          for (var j = 0; j < texts.drop[i].length; j++) {
            noFill()
            stroke(128 + 128 * sin(-Math.PI * 0.5 + Math.PI * animation.drop))
            strokeWeight(1)
            push()
            translate(windowWidth * 0.5 + (j - Math.floor(texts.drop[i].length * 0.5) + ((texts.drop[i].length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + (3 + i * 0.25 - 0.625 * (1 / 6.5)) * boardSize * maxSize * 6.5)
            drawLetters(texts.drop[i][j], boardSize * maxSize * 0.25, 0)
            pop()
          }
        }

        animation.drop += deltaTime * speed.drop
        if (animation.drop >= 1.99) {
          animation.drop = 0
          animationState.drop = false
          used.reset = true
        }
      }

      // move to the center
      if (animationState.center === true) {
        for (var i = 0; i < texts.center.length; i++) {
          for (var j = 0; j < texts.center[i].length; j++) {
            noFill()
            stroke(128 + 128 * sin(-Math.PI * 0.5 + Math.PI * animation.center))
            strokeWeight(1)
            push()
            translate(windowWidth * 0.5 + (j - Math.floor(texts.center[i].length * 0.5) + ((texts.center[i].length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + (3 + i * 0.25 - 0.625 * (1 / 6.5)) * boardSize * maxSize * 6.5)
            drawLetters(texts.center[i][j], boardSize * maxSize * 0.25, 0)
            pop()
          }
        }

        animation.center += deltaTime * speed.center
        if (animation.center >= 1.99) {
          animation.center = 0
        }
        if (animation.center === 0) {
          if (player.x === Math.floor(tutorialTiles.length * 0.5) && player.y === Math.floor(tutorialTiles.length * 0.5)) {
            animationState.center = false
            animationState.shrink = true
            states.tutorial = false
          }
        }
      }
    }
  }

  // shrinking
  if (animationState.shrink === true) {
    // place indicators
    for (var i = 0; i < initDimension; i++) {
      for (var j = 0; j < initDimension; j++) {
        noFill()
        stroke(255)
        strokeWeight(2)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(initDimension * 0.5)) * boardSize * maxSize * (1 - animation.shrink * 0.5) * 6.5, windowHeight * 0.5 + (j - Math.floor(initDimension * 0.5)) * boardSize * maxSize * (1 - animation.shrink * 0.5) * 6.5)
        drawTile(boardSize * maxSize * (1 - animation.shrink * 0.5) * 6.5 * 0.95)
        pop()
      }
    }
    for (var i = 0; i < shrinkTiles.length; i++) {
      for (var j = 0; j < shrinkTiles[i].length; j++) {
        if (shrinkTiles[i][j] === 1) {
          fill(255)
        } else {
          noFill()
        }
        stroke(255)
        strokeWeight(1)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(shrinkTiles.length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5), windowHeight * 0.5 + (j - Math.floor(shrinkTiles[i].length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5))
        drawTile(boardSize * maxSize * (1 - animation.shrink * 0.5) * 5)
        pop()
      }
    }

    // player
    push()
    translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5), windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5))
    fill(0)
    stroke(0)
    strokeWeight(1)
    drawTile(boardSize * maxSize * 3 * (1 - animation.shrink * 0.5))
    pop()

    if (animationState.shrink === true) {
      animation.shrink += deltaTime * speed.shrink
      if (animation.shrink >= 0.999) {
        animation.shrink = 1
        setTimeout(function() {
          animationState.shrink = false
          animationState.disappear = true
        }, 500)
      }
    }
  }

  // disappearing empty tiles
  if (animationState.disappear === true) {
    // place indicators
    for (var i = 0; i < initDimension; i++) {
      for (var j = 0; j < initDimension; j++) {
        noFill()
        stroke(255)
        strokeWeight(2)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(initDimension * 0.5)) * boardSize * initSize * 6.5, windowHeight * 0.5 + (j - Math.floor(initDimension * 0.5)) * boardSize * initSize * 6.5)
        drawTile(boardSize * initSize * 6.5 * 0.95)
        pop()
      }
    }
    for (var i = 0; i < initDimension; i++) {
      for (var j = 0; j < initDimension; j++) {
        fill(255)
        stroke(255)
        strokeWeight(1)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(initDimension * 0.5)) * boardSize * initSize * 6.5, windowHeight * 0.5 + (j - Math.floor(initDimension * 0.5)) * boardSize * initSize * 6.5)
        drawTile(boardSize * initSize * 5)
        pop()
      }
    }

    // player
    push()
    translate(windowWidth * 0.5 + (player.x - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5), windowHeight * 0.5 + (player.y - Math.floor(tutorialTiles.length * 0.5)) * boardSize * maxSize * 6.5 * (1 - animation.shrink * 0.5))
    fill(0)
    stroke(0)
    strokeWeight(1)
    drawTile(boardSize * maxSize * 3 * (1 - animation.shrink * 0.5))
    pop()

    for (var i = 0; i < disappearTiles.length; i++) {
      noFill()
      stroke(255)
      strokeWeight(1)
      if (disappearTiles[i][2] > 0) {
        push()
        translate(windowWidth * 0.5 + (disappearTiles[i][0] - Math.floor((maxDimension - 2) * 0.5) + 1) * boardSize * initSize * 6.5, windowHeight * 0.5 + (disappearTiles[i][1] - Math.floor((maxDimension - 2) * 0.5) + 1) * boardSize * initSize * 6.5)
        drawTile(boardSize * initSize * 5 * disappearTiles[i][2])
        pop()
      }
    }

    if (animationState.disappear === true) {
      animation.disappear += deltaTime * speed.disappear
      for (var i = 0; i < disappearTiles.length; i++) {
        if (animation.disappear > i) {
          disappearTiles[i][2] = 1 - (animation.disappear - i)
        }
        if (animation.disappear >= 18) {
          animationState.guidelines = true
        }
      }
    }

    if (animationState.guidelines === true) {
      // wall
      for (var i = 0; i < wallArray.length; i++) {
        for (var j = 0; j < wallArray[i].length; j++) {
          if (wallArray[i][j] === 1) {
            push()
            translate(windowWidth * 0.5 + (i - Math.floor(wallArray.length * 0.5)) * boardSize * initSize * scaling * 6.5, windowHeight * 0.5 + (j - Math.floor(wallArray[i].length * 0.5)) * boardSize * initSize * scaling * 6.5)
            noFill()
            stroke(255 * animation.guidelines)
            strokeWeight(1)
            drawTile(boardSize * initSize * scaling * 6.5)
            pop()

            push()
            translate(windowWidth * 0.5 + (i - Math.floor(wallArray.length * 0.5)) * boardSize * initSize * 6.5, windowHeight * 0.5 + (j - Math.floor(wallArray[i].length * 0.5)) * boardSize * initSize * 6.5)
            noFill()
            stroke(255 * animation.guidelines)
            strokeWeight(1)
            drawTile(boardSize * initSize * 0.95 * 6.5)
            pop()
          }
        }
      }

      // wall edges
      for (var i = 0; i < wallEdges.length; i++) {
        push()
        translate(windowWidth * 0.5, windowHeight * 0.5)
        noFill()
        stroke(255 * animation.guidelines)
        strokeWeight(1)
        line((wallEdges[i][0] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 * scaling, (wallEdges[i][1] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 * scaling, (wallEdges[i][0] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (wallEdges[i][1] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
        pop()
      }
    }

    if (animationState.guidelines === true) {
      animation.guidelines += deltaTime * speed.guidelines
      if (animation.guidelines >= 0.999) {
        setTimeout(function() {
          animationState.disappear = false
          animationState.guidelines = false
          states.gameplay = true
          player.x = Math.floor(maxDimension * 0.5)
          player.y = Math.floor(maxDimension * 0.5)
        }, 500)
      }
    }
  }

  // gameplay
  if (states.gameplay === true) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    if (states.paused === false && tileCount > 9) {
      factor.frame++
    }
    translate(sin(factor.frame * 0.02 * randX) * boardSize * initSize * factor.movement, cos(factor.frame * 0.02 * randY) * boardSize * initSize * factor.movement)
    rotate(factor.swing * sin(factor.frame * 0.0125))
    translate(Math.random() * boardSize * factor.shake, Math.random() * boardSize * factor.shake)

    // starfield
    push()
    translate(-sin(factor.frame * 0.02 * randX) * boardSize * initSize * factor.movement * 0.25, -cos(factor.frame * 0.02 * randY) * boardSize * initSize * factor.movement * 0.25)
    rotate(factor.swing * sin(factor.frame * 0.0125))
    for (var i = 0; i < stars.length; i++) {
      stars[i].display()
      if (stars[i].isOffScreen()) {
        stars.splice(i, 1)
      }
    }
    pop()

    // wall indicator
    for (var i = 0; i < wallArray.length; i++) {
      for (var j = 0; j < wallArray[i].length; j++) {
        if (wallArray[i][j] === 1) {
          push()
          translate((i - Math.floor(wallArray.length * 0.5)) * boardSize * initSize * 6.5, (j - Math.floor(wallArray[i].length * 0.5)) * boardSize * initSize * 6.5)
          noFill()
          stroke(255)
          strokeWeight(2)
          drawTile(boardSize * initSize * 6.5 * 0.95)
          pop()
        }
      }
    }

    // tiles
    for (var i = 0; i < tileArray.length; i++) {
      for (var j = 0; j < tileArray[i].length; j++) {
        if (tileArray[i][j] === 1) {
          fill(255, 1 - animation.endscreen - animation.over)
          stroke(255, 1 - animation.endscreen - animation.over)
          strokeWeight(1)
          push()
          translate((i - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (j - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
          drawTile(boardSize * initSize * 5)
          pop()
        }
      }
    }

    // player
    push()
    translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
    fill(0, 1 - animation.endscreen - animation.over)
    noStroke()
    drawTile(boardSize * initSize * 3)
    pop()

    // wall
    for (var i = 0; i < wallArray.length; i++) {
      for (var j = 0; j < wallArray[i].length; j++) {
        if (wallArray[i][j] === 1) {
          push()
          translate((i - Math.floor(wallArray.length * 0.5)) * boardSize * initSize * 6.5 * scaling, (j - Math.floor(wallArray[i].length * 0.5)) * boardSize * initSize * 6.5 * scaling)
          noFill()
          stroke(255, 1 - animation.endscreen - animation.over)
          strokeWeight(1)
          drawTile(boardSize * initSize * scaling * 6.5)
          pop()
        }
      }
    }

    // wall edges
    for (var i = 0; i < wallEdges.length; i++) {
      push()
      noFill()
      stroke(255, 1 - animation.endscreen - animation.over)
      strokeWeight(1)
      line((wallEdges[i][0] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 * scaling, (wallEdges[i][1] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 * scaling, (wallEdges[i][0] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (wallEdges[i][1] - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
      pop()
    }

    // grabbed tile
    if (states.tileGrabbed === true) {
      push()
      translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
      noStroke()
      fill(255, 1 - animation.endscreen - animation.over)
      drawTile(boardSize * initSize)
      pop()
    }

    // neighbour grab arrows
    if (states.grabNeighbour === true) {
      if (neighbours[0] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 + boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 1)
        pop()
      }
      if (neighbours[1] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 - boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 3)
        pop()
      }
      if (neighbours[2] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 + boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 2)
        pop()
      }
      if (neighbours[3] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 - boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 0)
        pop()
      }
    }

    // tile placement arrows
    if (states.tileGrabbed === true) {
      neighbours = getNeighbours(player.x, player.y, tileArray, 0, 0, 0)
      if (neighbours[0] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 + boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 1)
        pop()
      }
      if (neighbours[1] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 - boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 3)
        pop()
      }
      if (neighbours[2] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 + boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 2)
        pop()
      }
      if (neighbours[3] === true) {
        push()
        translate((Math.floor(player.x) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5, (Math.floor(player.y) - Math.floor(maxDimension * 0.5)) * boardSize * initSize * 6.5 - boardSize * initSize * 6.5)
        fill(128, 1 - animation.endscreen - animation.over)
        noStroke()
        drawArrow(boardSize * initSize * 1.25, 0)
        pop()
      }
    }
    pop()
    pop()

    // paused
    if (states.paused === true) {
      for (var i = 0; i < texts.paused.length; i++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.paused.length * 0.5) + ((texts.paused.length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 2, windowHeight * 0.5)
        fill(0)
        stroke(255)
        strokeWeight(1)
        drawLetters(texts.paused[i], boardSize * initSize * 2, 0)
        pop()
      }

      // resume / pause text
      for (var i = 0; i < texts.resume.length; i++) {
        fill(255, 0.5 + 0.5 * sin(animation.resume))
        noStroke()
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.resume.length * 0.5) + ((texts.resume.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 2 * boardSize * maxSize * 6.5)
        drawLetters(texts.resume[i], boardSize * maxSize * 0.25, 0)
        pop()
      }
      animation.resume += deltaTime * speed.resume
    } else {
      randX = noise(Math.random() * 0.01)
      randY = noise(Math.random() * 0.01)
    }

    // matches to go
    if (animationState.togo === true && matches !== 0) {
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      noFill()
      fill(0, 1 - animation.togo)
      stroke(255, 1 - animation.togo)
      strokeWeight(1)
      drawLetters((matchesForCombo - matches).toString(), boardSize * initSize * 6.5 * (1 + animation.togo), 0)

      animation.togo += speed.togo
      if (animation.togo >= 0.9999) {
        animationState.togo = false
        animation.togo = 0
      }
    }

    // combo
    if (animationState.combo === true) {
      for (var i = 0; i < texts.combo.length; i++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(texts.combo.length * 0.5) + ((texts.combo.length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 2.5, windowHeight * 0.5)
        fill(0, 1 - animation.combo)
        stroke(255, 1 - animation.combo)
        strokeWeight(1)
        drawLetters(texts.combo[i], boardSize * initSize * 2.5, 0)
        pop()
      }

      animation.combo += speed.combo
      if (animation.combo >= 0.9999) {
        animationState.combo = false
        animation.combo = 0
      }
    }

    // collision
    if (animationState.collision === true) {
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      fill(0, 1 - animation.collision)
      stroke(255, 1 - animation.collision)
      strokeWeight(1)
      drawLetters('x', boardSize * initSize * 6.5 * (1 + animation.collision), 0)
      pop()

      animation.collision += speed.collision
      if (animation.collision >= 0.9999) {
        animationState.collision = false
        animation.collision = 0
      }
    }

    // computations, etc.
    if (states.paused === false) {
      newStar += deltaTime * 0.01
      if (newStar >= 1) {
        newStar = 0
        makeStar()
      }
      for (var i = 0; i < stars.length; i++) {
        stars[i].move()
      }

      scaling -= deltaTime * speed.scaling
      if (scaling < 1) {
        scaling = maxScale
        collided = wallCollision(wallArray, tileArray)
        if (playerChecker(player.x, player.y, wallArray) === true) {
          animationState.over = true
        } else {
          if (collided.length > 0) {
            matches = 0
            animationState.collision = true
            for (var i = 0; i < collided.length; i++) {
              tileArray[collided[i][0]][collided[i][1]] = 0
            }
            collided = []
            wallArray = deepCopy(tileArray)
            wallEdges = getVertices(wallArray)
          } else {
            if (allDeepEqual([tileArray, wallArray]) === true) {
              matches++
            }
            animationState.togo = true
            if (matches === matchesForCombo) {
              additionalTiles = getEmptyNeighbours(getTiles(tileArray), tileArray)
              tileToAdd = additionalTiles[Math.floor(Math.random() * additionalTiles.length)]
              tileArray[tileToAdd[0]][tileToAdd[1]] = 1
              wallArray = deepCopy(tileArray)
              wallEdges = getVertices(wallArray)
              rearrangeTiles(wallArray, 1)
              wallEdges = getVertices(wallArray)
              additionalTiles = []
              tileToAdd = []
              matches = 0
              animationState.combo = true
            } else {
              rearrangeTiles(wallArray, 1)
              wallEdges = getVertices(wallArray)
            }
          }
        }
        tileCount = tileCounter(tileArray, 1)

        if (tileCount === endNumOfTiles + 1) {
          animationState.endscreen = true
        }
        if (tileCount > 9) {
          factor.movement = (numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * endValue.movement
          factor.swing = (numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * endValue.swing
          factor.shake = (numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * endValue.shake
          factor.numOfStars = 4 + Math.floor((numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * ((endValue.numOfStars + 1) - initValue.numOfStars))
          factor.randS = initValue.randS + (numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * (endValue.randS - initValue.randS)
          speed.scaling = initValue.scaling + (numOfLevels - (endNumOfTiles - tileCount)) / numOfLevels * (endValue.scaling - initValue.scaling)
        }
        if (tileCount <= 9) {
          factor.movement = initValue.movement
          factor.swing = initValue.swing
          factor.shake = initValue.shake
          factor.numOfStars = initValue.numOfStars
          factor.randS = initValue.randS
          speed.scaling = initValue.scaling
        }
      }

      // fade to game over
      if (animationState.over === true) {
        animation.over += deltaTime * speed.over
        if (animation.over >= 0.999) {
          animationState.over = false
          states.gameplay = false
          states.over = true
          animation.over = 0
        }
      }

      // fade to endscreen
      if (animationState.endscreen === true) {
        animation.endscreen += deltaTime * speed.endscreen
        if (animation.endscreen >= 0.999) {
          animationState.endscreen = false
          states.gameplay = false
          states.endscreen = true
          animation.endscreen = 0
        }
      }
    }
  }

  // game over
  if (states.over === true) {
    // game over text
    for (var i = 0; i < texts.over.length; i++) {
      for (var j = 0; j < texts.over[i].length; j++) {
        push()
        translate(windowWidth * 0.5 + (j - Math.floor(texts.over[i].length * 0.5) + ((texts.over[i].length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 3, windowHeight * 0.5 + (i - Math.floor(texts.over.length * 0.5) + ((texts.over.length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 3)
        fill(0)
        stroke(255)
        strokeWeight(1)
        drawLetters(texts.over[i][j], boardSize * initSize * 3, 0)
        pop()
      }
    }
    // restart text
    for (var i = 0; i < texts.restart.length; i++) {
      fill(255, 0.5 + 0.5 * sin(animation.restart))
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(texts.restart.length * 0.5) + ((texts.restart.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 2 * boardSize * maxSize * 6.5)
      drawLetters(texts.restart[i], boardSize * maxSize * 0.25, 0)
      pop()
    }
    animation.restart += deltaTime * speed.restart
  }

  // endscreen
  if (states.endscreen === true) {
    // endscreen text
    for (var i = 0; i < texts.endscreen.length; i++) {
      for (var j = 0; j < texts.endscreen[i].length; j++) {
        push()
        translate(windowWidth * 0.5 + (j - Math.floor(texts.endscreen[i].length * 0.5) + ((texts.endscreen[i].length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 4, windowHeight * 0.5 + (i - Math.floor(texts.endscreen.length * 0.5) + ((texts.endscreen.length + 1) % 2) * 0.5) * boardSize * initSize * 6.5 * 4 - boardSize * initSize * 6.5 * 1)
        fill(0)
        stroke(255)
        strokeWeight(1)
        drawLetters(texts.endscreen[i][j], boardSize * initSize * 4, 0)
        pop()
      }
    }
    // restart text
    for (var i = 0; i < texts.restart.length; i++) {
      fill(255, 0.5 + 0.5 * sin(animation.restart))
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(texts.restart.length * 0.5) + ((texts.restart.length + 1) % 2) * 0.5) * boardSize * maxSize * 6.5 * 0.25, windowHeight * 0.5 + 2 * boardSize * maxSize * 6.5)
      drawLetters(texts.restart[i], boardSize * maxSize * 0.25, 0)
      pop()
    }
    animation.restart += deltaTime * speed.restart
  }
}

function keyPressed() {
  // enter the game
  if (states.title === true) {
    // space
    if (keyCode === 32) {
      animationState.title = true
    }
  }

  // tutorial
  if (states.tutorial === true) {
    if (animationState.intro === false) {
      // player's movement
      if (states.grabNeighbour === false) {
        if (keyCode === RIGHT_ARROW) {
          if (player.x < tutorialTiles.length - 1) {
            if (tutorialTiles[player.x + 1][player.y] === 1) {
              player.x++
              if (animationState.move === false && used.move === false) {
                animationState.grab = true
                used.move = true
              }
              if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
                animationState.center = true
              }
            }
          }
        }
        if (keyCode === LEFT_ARROW) {
          if (player.x > 0) {
            if (tutorialTiles[player.x - 1][player.y] === 1) {
              player.x--
              if (animationState.move === false && used.move === false) {
                animationState.grab = true
                used.move = true
              }
              if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
                animationState.center = true
              }
            }
          }
        }
        if (keyCode === DOWN_ARROW) {
          if (player.y < tutorialTiles.length - 1) {
            if (tutorialTiles[player.x][player.y + 1] === 1) {
              player.y++
              if (animationState.move === false && used.move === false) {
                animationState.grab = true
                used.move = true
              }
              if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
                animationState.center = true
              }
            }
          }
        }
        if (keyCode === UP_ARROW) {
          if (player.y > 0) {
            if (tutorialTiles[player.x][player.y - 1] === 1) {
              player.y--
              if (animationState.move === false && used.move === false) {
                animationState.grab = true
                used.move = true
              }
              if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
                animationState.center = true
              }
            }
          }
        }
      }
      // player's action (grabbing neighbouring cell)
      if (states.grabNeighbour === true) {
        if (keyCode === RIGHT_ARROW && neighbours[0] === true) {
          setTimeout(function() {
            tutorialTiles[player.x + 1][player.y] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === LEFT_ARROW && neighbours[1] === true) {
          setTimeout(function() {
            tutorialTiles[player.x - 1][player.y] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === DOWN_ARROW && neighbours[2] === true) {
          setTimeout(function() {
            tutorialTiles[player.x][player.y + 1] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === UP_ARROW && neighbours[3] === true) {
          setTimeout(function() {
            tutorialTiles[player.x][player.y - 1] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
      }
      // player's action (placing to neighbouring cell)
      if (states.tileGrabbed === true) {
        if (keyCode === RIGHT_ARROW && neighbours[0] === true) {
          setTimeout(function() {
            tutorialTiles[player.x + 1][player.y] = 1
            states.tileGrabbed = false
            if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
              animationState.center = true
            }
          }, 1)
        }
        if (keyCode === LEFT_ARROW && neighbours[1] === true) {
          setTimeout(function() {
            tutorialTiles[player.x - 1][player.y] = 1
            states.tileGrabbed = false
            if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
              animationState.center = true
            }
          }, 1)
        }
        if (keyCode === DOWN_ARROW && neighbours[2] === true) {
          setTimeout(function() {
            tutorialTiles[player.x][player.y + 1] = 1
            states.tileGrabbed = false
            if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
              animationState.center = true
            }
          }, 1)
        }
        if (keyCode === UP_ARROW && neighbours[3] === true) {
          setTimeout(function() {
            tutorialTiles[player.x][player.y - 1] = 1
            states.tileGrabbed = false
            if (allDeepEqual([tutorialTiles, shrinkTiles]) === true && animationState.drop === false) {
              animationState.center = true
            }
          }, 1)
        }
      }
      // space
      if (keyCode === 32) {
        if (states.grabNeighbour === false && states.tileGrabbed === false) {
          states.grabNeighbour = true
          neighbours = getNeighbours(player.x, player.y, tutorialTiles, 1, 0, 0)
          if (animationState.grab === false && used.reset === false) {
            animationState.drop = true
          }
        }
      }
    }
  }

  // gameplay
  if (states.gameplay === true) {
    if (states.paused === false) {
      // player's movement
      if (states.grabNeighbour === false) {
        if (keyCode === RIGHT_ARROW) {
          if (player.x < maxDimension - 1) {
            if (tileArray[player.x + 1][player.y] === 1) {
              player.x++
            }
          }
        }
        if (keyCode === LEFT_ARROW) {
          if (player.x > 0) {
            if (tileArray[player.x - 1][player.y] === 1) {
              player.x--
            }
          }
        }
        if (keyCode === DOWN_ARROW) {
          if (player.y < maxDimension - 1) {
            if (tileArray[player.x][player.y + 1] === 1) {
              player.y++
            }
          }
        }
        if (keyCode === UP_ARROW) {
          if (player.y > 0) {
            if (tileArray[player.x][player.y - 1] === 1) {
              player.y--
            }
          }
        }
      }
      // player's action (grabbing neighbouring cell)
      if (states.grabNeighbour === true) {
        if (keyCode === RIGHT_ARROW && neighbours[0] === true) {
          setTimeout(function() {
            tileArray[player.x + 1][player.y] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === LEFT_ARROW && neighbours[1] === true) {
          setTimeout(function() {
            tileArray[player.x - 1][player.y] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === DOWN_ARROW && neighbours[2] === true) {
          setTimeout(function() {
            tileArray[player.x][player.y + 1] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
        if (keyCode === UP_ARROW && neighbours[3] === true) {
          setTimeout(function() {
            tileArray[player.x][player.y - 1] = 0
            states.tileGrabbed = true
            states.grabNeighbour = false
          }, 1)
        }
      }
      // player's action (placing to neighbouring cell)
      if (states.tileGrabbed === true) {
        if (keyCode === RIGHT_ARROW && neighbours[0] === true) {
          setTimeout(function() {
            tileArray[player.x + 1][player.y] = 1
            states.tileGrabbed = false
          }, 1)
        }
        if (keyCode === LEFT_ARROW && neighbours[1] === true) {
          setTimeout(function() {
            tileArray[player.x - 1][player.y] = 1
            states.tileGrabbed = false
          }, 1)
        }
        if (keyCode === DOWN_ARROW && neighbours[2] === true) {
          setTimeout(function() {
            tileArray[player.x][player.y + 1] = 1
            states.tileGrabbed = false
          }, 1)
        }
        if (keyCode === UP_ARROW && neighbours[3] === true) {
          setTimeout(function() {
            tileArray[player.x][player.y - 1] = 1
            states.tileGrabbed = false
          }, 1)
        }
      }
      // space
      if (keyCode === 32) {
        if (states.grabNeighbour === false && states.tileGrabbed === false) {
          states.grabNeighbour = true
          neighbours = getNeighbours(player.x, player.y, tileArray, 1, 0, 0)
        }
      }
    }
    // pause / resume
    if (states.paused === true) {
      if (keyCode === 80) {
        states.paused = false
      }
    } else {
      if (keyCode === 80) {
        states.paused = true
      }
    }
  }

  // restart (game over)
  if (states.over === true) {
    if (keyCode === 32) {
      player = {
        x: Math.floor(maxDimension * 0.5),
        y: Math.floor(maxDimension * 0.5)
      }
      tileArray = create2DArray(maxDimension, maxDimension, 0, true)
      for (var i = 0; i < initDimension; i++) {
        for (var j = 0; j < initDimension; j++) {
          tileArray[Math.floor(maxDimension * 0.5 - 1) + i][Math.floor(maxDimension * 0.5 - 1) + j] = 1
        }
      }
      wallArray = deepCopy(tileArray)
      wallEdges = getVertices(wallArray)

      if (windowWidth >= windowHeight) {
        boardSize = windowHeight - 80
        maxScale = windowHeight / (boardSize * initSize) / 6.5
      } else {
        boardSize = windowWidth - 80
        maxScale = windowWidth / (boardSize * initSize) / 6.5
      }
      scaling = maxScale

      animation.restart = 0
      states.over = false
      states.gameplay = true

      factor.movement = 0
      factor.swing = 0
      factor.shake = 0
      factor.numOfStars = 4
      factor.randS = 0.00025
      speed.scaling = 0.0005

      stars = []
      newStar = 0
    }
  }

  // restart (endscreen)
  if (states.endscreen === true) {
    if (keyCode === 32) {
      player = {
        x: Math.floor(tutorialTiles.length * 0.5),
        y: Math.floor(tutorialTiles.length * 0.5)
      }
      tileArray = create2DArray(maxDimension, maxDimension, 0, true)
      for (var i = 0; i < initDimension; i++) {
        for (var j = 0; j < initDimension; j++) {
          tileArray[Math.floor(maxDimension * 0.5 - 1) + i][Math.floor(maxDimension * 0.5 - 1) + j] = 1
        }
      }
      wallArray = deepCopy(tileArray)
      wallEdges = getVertices(wallArray)

      if (windowWidth >= windowHeight) {
        boardSize = windowHeight - 80
        maxScale = windowHeight / (boardSize * initSize) / 6.5
      } else {
        boardSize = windowWidth - 80
        maxScale = windowWidth / (boardSize * initSize) / 6.5
      }
      scaling = maxScale

      animation.restart = 0
      states.endscreen = false
      states.title = true

      factor.movement = 0
      factor.swing = 0
      factor.shake = 0
      factor.numOfStars = 4
      factor.randS = 0.00025
      speed.scaling = 0.0005

      stars = []
      newStar = 0
    }
  }

  if (keyCode === 77) {
    drums.stop()
  }
  if (keyCode === 83) {
    drums.start()
  }
}

function keyReleased() {
  // tutorial
  if (states.tutorial === true) {
    if (keyCode === 32) {
      states.grabNeighbour = false
      neighbours = getNeighbours(player.x, player.y, tutorialTiles, 1, 0, 1)
    }
  }

  // gameplay
  if (states.gameplay === true) {
    if (keyCode === 32) {
      states.grabNeighbour = false
      neighbours = []
    }
  }
}

window.addEventListener('focus', function(event) {
  states.focused = true
}, false)

window.addEventListener('blur', function(event) {
  states.focused = false
  states.paused = true
}, false)

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    maxScale = windowHeight / (boardSize * initSize) / 6.5
  } else {
    boardSize = windowWidth - 80
    maxScale = windowWidth / (boardSize * initSize) / 6.5
  }
  createCanvas(windowWidth, windowHeight)
}
